import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:my_other_app/screens/splash_screen.dart';

void main() {
  runApp(GoodApp());
}

class GoodApp extends StatelessWidget {
  Color _primaryColor = HexColor('#D133FE');
  Color _accentColor = HexColor('#8451AA');

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Good App',
      theme: ThemeData(
          primaryColor: _primaryColor,
          scaffoldBackgroundColor: Color.fromARGB(255, 186, 224, 240),
          colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.grey)
              .copyWith(secondary: _accentColor)),
      home: SplashScreen(
        title: 'The Good App',
      ),
    );
  }
}
