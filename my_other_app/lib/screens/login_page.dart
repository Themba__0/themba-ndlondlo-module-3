import 'package:flutter/material.dart';
import 'package:my_other_app/screens/common/theme_helper.dart';
import 'package:my_other_app/screens/widgets/header_widget.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  double headerHeight = 250;
  Key _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: headerHeight,
              child: HeaderWidget(headerHeight, true, Icons.login_rounded),
            ),
            SafeArea(
              child: Container(
                child: Column(
                  children: [
                    Text(
                      'Welcome back',
                      style: TextStyle(
                        fontSize: 60,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    SizedBox(height: 30.0),
                    Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextField(
                                decoration: ThemeHelper().textInputDecoration(
                                    'Username', 'Enter your email:')),
                            SizedBox(height: 30.0),
                            TextField(
                                obscureText: true,
                                decoration: ThemeHelper().textInputDecoration(
                                    'Password', 'Enter your password:')),
                            SizedBox(height: 15.0),
                            Container(
                              child: Text('Forgot Password'),
                            ),
                            Container(
                              child: Text('Sign in'),
                            ),
                            Container(
                              child: Text('New user? Sign up'),
                            ),
                          ],
                        ))
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
