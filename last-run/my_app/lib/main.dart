import 'package:flutter/material.dart';
import 'package:my_app/my_home.dart';
import 'package:my_app/register.dart';
import 'package:my_app/user_dashboard.dart';

void main() {
  runApp(
    MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: 'login',
        routes: {
          'login': ((context) => MyHome()),
          'register': ((context) => register_page()),
          'dashboard': ((context) => userDashboard()),
        }),
  );
}
