import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login_app',
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  //form key
  final _formKey = GlobalKey<FormState>();
  //editing controller
  final TextEditingController emailContr = new TextEditingController();
  final TextEditingController passContr = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    //email field
    final emailField = TextFormField(
      autofocus: false,
      controller: emailContr,
      keyboardType: TextInputType.emailAddress,
      //validator: (){},
      onSaved: (value) {
        emailContr.text = value!;
      },
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.mail),
        contentPadding: EdgeInsets.fromLTRB(0, 15, 20, 25),
        hintText: 'Email address',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );

    // Password field
    final passwordField = TextFormField(
      autofocus: false,
      controller: passContr,
      obscureText: true,

      //validator: (){},
      onSaved: (value) {
        passContr.text = value!;
      },
      textInputAction: TextInputAction.done,
      decoration: InputDecoration(
        prefixIcon: Icon(Icons.vpn_key),
        contentPadding: EdgeInsets.fromLTRB(0, 15, 20, 25),
        hintText: 'Password',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
      //return Container();
    );
    final loginButton = Material(
        elevation: 2.5,
        borderRadius: BorderRadius.circular(5),
        
        color: Color.fromARGB(255, 209, 103, 135),
        child: MaterialButton(
          padding: EdgeInsets.fromLTRB(0, 15, 20, 25),
          minWidth: MediaQuery.of(context).size.width,
          onPressed: (){},
          child: Text('Login',
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
            color: Colors.white,
            ),
            ),
          ),
          
    );
    return Scaffold(
      //backgroundColor: Color.fromARGB(255, 139, 160, 125),
      body: Center(
          child: SingleChildScrollView(
        child: Container(
        color: Color.fromARGB(255, 244, 245, 242),
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>
                [
                  SizedBox(height: 200,
                  child: Image.asset('images/my_logo.jpg', fit: BoxFit.contain,),
                  
                  ),
                   emailField, passwordField,loginButton],
              ),
            ),
          ),
        ),
      )),
    );
  }
}
